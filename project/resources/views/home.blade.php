@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    @if(Auth::check())
                        <div class="alert alert-success">
                            From- {{ Auth::user()->Address->street }}
                        </div>
                    @endif

                    @foreach(Auth::user()->posts as $post)
                        <div class="card mt-3">
                            <div class="card-header">
                                <h3>{{ $post->title }}</h3>
                                <p>This Post in <b>{{ $post->category->name }}</b> category</p>
                                <p>Posted By  <b>{{ $post->user->name }}</b> </p>
                            </div>
                            <div class="card-body">
                                {{ $post->description }}
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
