@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                @foreach($posts as $post)
                    <div class="card mt-3">
                        <div class="card-header">
                            <h3>{{ $post->title }}</h3>
                            <p>This Post in <b>{{ $post->category->name }}</b> category</p>
                            <p>Posted By  <b>{{ $post->user->name }}</b> </p>
                        </div>
                        <div class="card-body">
                            <div>
                                @foreach($post->images as $image)
                                    <img src='{{ asset("/images/$image->image") }}'>
                                @endforeach
                            </div>
                            {{ $post->description }}
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@endsection
