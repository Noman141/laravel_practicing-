<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            <form action="{{ route('store') }}" method="post" enctype="multipart/form-data">
               {{ csrf_field() }}
                <input type="text" name="title" id="" placeholder="Name"><br/>
                <input type="file" name="image" id=""><br/>
                <input type="submit" value="Submit">
            </form>
<br/>
            <table>
                <tr>
                    <th>#</th>
                    <th>Title</th>
                    <th>Image</th>
                    <th>Action</th>
                </tr>
                @php $i= 0 @endphp
                @foreach($images as $image)
                    @php $i++; @endphp
                <tr>
                    <td>{{ $i }}</td>
                    <td>{{ $image->title }}</td>
                    <td>
                        <img src="{{ Storage::url($image->image) }}" alt="" width="100px">
                    </td>
                    <td>
                        <a href="">Edit</a>
                    </td>
                </tr>
                @endforeach
            </table>
        </div>
    </body>
</html>
