<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;

class PagesController extends Controller{
    public function index(){
        $posts = Post::orderBy('id','desc')->paginate(1);
        return view('index')->with('posts',$posts);
    }

    public function search(Request $request){
        $this->validate($request,[
            'search' => 'required'
        ]);

        $search_txt = $request->search;

        $posts = Post::orderBy('id','desc')
                ->where('title','like','%'.$search_txt.'%')
                ->orwhere('description','like','%'.$search_txt.'%')
                ->get();
        return view('search')->with('posts',$posts);
    }
}
